/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Models.Datos;
import Views.EditFrame;
import Views.RegistroFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;

/**
 *
 * @author Kenneth Granados
 */
public class EditController implements ActionListener{
    EditFrame eframe;
     private Datos datos;
    JFileChooser jfc;
   
    public EditController(EditFrame ef) {
        this.eframe = ef;
        jfc = new JFileChooser();
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
    switch(ae.getActionCommand()){
         case "load":
             jfc.showOpenDialog(eframe);
             datos = open(jfc.getSelectedFile());
             eframe.setData(datos);
             break;
         case "edit":
             jfc.showSaveDialog(eframe);
             datos = eframe.getEditData();
             save(jfc.getSelectedFile());
             break;
    }
    } 
    
     public void save(File file){
    try {
        ObjectOutputStream w = new ObjectOutputStream (new FileOutputStream(file));
        w.writeObject(datos);
        w.flush();
    } catch (IOException e) {
        System.out.println(e.getMessage());
    }
     }
     
    public Datos open(File file){
   try {
        ObjectInputStream ois = new ObjectInputStream (new FileInputStream(file));
        return (Datos)ois.readObject();
    } catch (IOException | ClassNotFoundException e) {
        System.out.println(e.getMessage());
    } 
   return null;
}
    
}
