/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Views.EditFrame;
import Views.MainFrame;
import Views.RegistroFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Kenneth Granados
 */
public class MainController implements ActionListener {
    MainFrame mframe;

    public MainController(MainFrame mframe) {
        this.mframe = mframe;
    }

    
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        switch(ae.getActionCommand()){
         case "exit":
             System.exit(0);
             break;
         case "Registrar":
             invokeRegistrar();
             break;
         case "Editar":
             invokeEditar();
             break;
         default:
             break;
             
    }
    }
//    public void invokecreate(){
//       BookFrame bf = new BookFrame();
//       frame.showChild(bf,false) ;       
//   } 
     public void invokeRegistrar(){
        RegistroFrame rframe = new RegistroFrame();
        mframe.showChild(rframe,false) ;
        
    }
     
      public void invokeEditar(){
        EditFrame eframe = new EditFrame();
        mframe.showChild(eframe,false) ;
    }
     
}
