/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Models.Datos;
import Views.RegistroFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;

/**
 *
 * @author Kenneth Granados
 */
public class RegistroController implements ActionListener{
  private RegistroFrame rframe;
  private Datos datos;
  JFileChooser jfc;

    public RegistroController() {
    }
  
  
    public RegistroController(RegistroFrame rframe) {
        this.rframe = rframe;
        jfc = new JFileChooser();
    }
  
  
    @Override
    public void actionPerformed(ActionEvent ae) {
       switch(ae.getActionCommand()){
         case "clean":
             rframe.clean();
             break;
         case "save":
             jfc.showSaveDialog(rframe);
             datos = rframe.getData();
             save(jfc.getSelectedFile());
             break;
    }
    } 
    public void save(File file){
    try {
        ObjectOutputStream w = new ObjectOutputStream (new FileOutputStream(file));
        w.writeObject(datos);
        w.flush();
    } catch (IOException e) {
       
    }
}
}