/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;

/**
 *
 * @author Kenneth Granados
 */
public class Datos implements Serializable{
    String primerNombre,segundoNombre,primerApellido,segundoApellido,oficio,fechadeNacimiento,fechadContratacion;
    int añosdeExperiencia;

    public Datos() {
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getOficio() {
        return oficio;
    }

    public void setOficio(String oficio) {
        this.oficio = oficio;
    }

    public String getFechadeNacimiento() {
        return fechadeNacimiento;
    }

    public void setFechadeNacimiento(String fechadeNacimiento) {
        this.fechadeNacimiento = fechadeNacimiento;
    }

    public String getFechadContratacion() {
        return fechadContratacion;
    }

    public void setFechadContratacion(String fechadContratacion) {
        this.fechadContratacion = fechadContratacion;
    }

    public int getAñosdeExperiencia() {
        return añosdeExperiencia;
    }

    public void setAñosdeExperiencia(int añosdeExperiencia) {
        this.añosdeExperiencia = añosdeExperiencia;
    }
    
    
}
