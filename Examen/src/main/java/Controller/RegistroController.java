/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Models.Datos;
import Views.Registro;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;

/**
 *
 * @author LABORATORIO_CCBB #9
 */
public class RegistroController implements ActionListener {
    private Registro registro,r;
    private Datos datos;
     JFileChooser jf;
    
    public RegistroController(Registro re) {
        this.r = re;
    }
    
   
    @Override
    public void actionPerformed(ActionEvent e) {
       switch(e.getActionCommand()){
          case"save":
             jf.showSaveDialog(registro);
             File file = jf.getSelectedFile();
             datos = registro.getData();
             save(file);
              break;
    }
    } 
    public void save(File file){
    try {
        ObjectOutputStream w = new ObjectOutputStream (new FileOutputStream(file));
        w.writeObject(datos);
        w.flush();
    } catch (IOException e) {
       
    }
}
}
