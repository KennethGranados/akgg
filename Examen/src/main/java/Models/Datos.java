/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author LABORATORIO_CCBB #9
 */
public class Datos {
    String PrimerNombre, SegundoNombre, PrimerApellido, SegundoApellido, Fechadenacimiento, Oficio, Fechadecontratación;
    int Añosdeexperiencia;

    public Datos() {
    }

    public String getPrimerNombre() {
        return PrimerNombre;
    }

    public void setPrimerNombre(String PrimerNombre) {
        this.PrimerNombre = PrimerNombre;
    }

    public String getSegundoNombre() {
        return SegundoNombre;
    }

    public void setSegundoNombre(String SegundoNombre) {
        this.SegundoNombre = SegundoNombre;
    }

    public String getPrimerApellido() {
        return PrimerApellido;
    }

    public void setPrimerApellido(String PrimerApellido) {
        this.PrimerApellido = PrimerApellido;
    }

    public String getSegundoApellido() {
        return SegundoApellido;
    }

    public void setSegundoApellido(String SegundoApellido) {
        this.SegundoApellido = SegundoApellido;
    }

    public String getFechadenacimiento() {
        return Fechadenacimiento;
    }

    public void setFechadenacimiento(String Fechadenacimiento) {
        this.Fechadenacimiento = Fechadenacimiento;
    }

    public String getOficio() {
        return Oficio;
    }

    public void setOficio(String Oficio) {
        this.Oficio = Oficio;
    }

    public String getFechadecontratación() {
        return Fechadecontratación;
    }

    public void setFechadecontratación(String Fechadecontratación) {
        this.Fechadecontratación = Fechadecontratación;
    }

    public int getAñosdeexperiencia() {
        return Añosdeexperiencia;
    }

    public void setAñosdeexperiencia(int Añosdeexperiencia) {
        this.Añosdeexperiencia = Añosdeexperiencia;
    }
    
    
}
